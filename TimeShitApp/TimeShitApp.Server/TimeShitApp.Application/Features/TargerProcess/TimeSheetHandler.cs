using IdentityManager.Shared.Shared;
using MediatR;
using Microsoft.Extensions.Logging;
using TimeShitApp.Application.ServicesInterfaces;
using TimeShitApp.Share;

namespace TimeShitApp.Application.Features.TargerProcess;

public sealed record TimeDataRequest(DateTime? StartTime, DateTime? EndTime, string userId, string authorization) : IRequest<Result<TimeDataResponse>>;

public sealed record TimeDataResponse(IList<Time> Times);

public sealed class TimeSheetHandler : IRequestHandler<TimeDataRequest, Result<TimeDataResponse>>
{
    private readonly ITPService _tpService;

    public TimeSheetHandler(ITPService tpService)
    {
        _tpService = tpService;
    }

    public async Task<Result<TimeDataResponse>> Handle(TimeDataRequest request, CancellationToken cancellationToken)
    {
        var times = await _tpService.GetTimes(request.StartTime, request.EndTime, request.userId, request.authorization);

        return new TimeDataResponse(times);
    }
}