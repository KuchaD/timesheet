using Carter;
using MediatR;
using Serilog;
using Serilog.Context;
using TimeShit.WebAssembly.Shared;
using TimeShit.WebAssembly.Shared.Models;
using TimeShitApp.Application.Features.TargerProcess;
using Utils = TimeShitApp.Application.Utils;

namespace TimeShit.WebAssembly.Server;

public class ApiModule : ICarterModule
{
    public void AddRoutes(IEndpointRouteBuilder app)
    {
        app.MapPost("/api/login",
            async (LoginModel loginModel, IMediator mediator, HttpContext httpContext, CancellationToken cancellationToken) =>
            {
                var result = await mediator.Send(new UserDataRequest(loginModel.Username, loginModel.Password),
                    cancellationToken);

                if (result.IsFailure)
                {
                    return Results.Problem();
                }
                
                var uBasicAuth = Utils.CreateBasicAuth(loginModel.Username, loginModel.Password);
                var claims = new List<Claim>
                {
                    new (ClaimTypes.Name, result.Value.User.Name),
                    new (ClaimNames.Basic, uBasicAuth),
                    new (ClaimTypes.Surname, result.Value.User.Surname),
                    new (ClaimTypes.Email, result.Value.User.Email),
                    new (ClaimNames.UserId, result.Value.User.id)
                };

                var claimsIdentity = new ClaimsIdentity(
                    claims, CookieAuthenticationDefaults.AuthenticationScheme);

                var authProperties = new AuthenticationProperties
                {
                    //AllowRefresh = <bool>,
                    // Refreshing the authentication session should be allowed.

                    //ExpiresUtc = DateTimeOffset.UtcNow.AddMinutes(10),
                    // The time at which the authentication ticket expires. A 
                    // value set here overrides the ExpireTimeSpan option of 
                    // CookieAuthenticationOptions set with AddCookie.

                    //IsPersistent = true,
                    // Whether the authentication session is persisted across 
                    // multiple requests. When used with cookies, controls
                    // whether the cookie's lifetime is absolute (matching the
                    // lifetime of the authentication ticket) or session-based.

                    //IssuedUtc = <DateTimeOffset>,
                    // The time at which the authentication ticket was issued.

                    //RedirectUri = <string>
                    // The full path or absolute URI to be used as an http 
                    // redirect response value.
                };

                await httpContext.SignInAsync(
                    CookieAuthenticationDefaults.AuthenticationScheme,
                    new ClaimsPrincipal(claimsIdentity),
                    authProperties);

                return Results.Ok();
            });

        app.MapGet("/api/logout", async (HttpContext httpContext, CancellationToken cancellationToken) =>
        {
            await httpContext.SignOutAsync();
        });

        app.MapGet("/api/time", async ([FromQuery] DateTime startTime, [FromQuery] DateTime endTime,IMediator mediator, HttpContext httpContext, ILoggerFactory loggerFactory, CancellationToken cancellationToken) =>
        {
            var logger = loggerFactory.CreateLogger("Time");
            var userId = httpContext.User.Claims.First(x => x.Type == ClaimNames.UserId).Value;
            var auth = httpContext.User.Claims.First(x => x.Type == ClaimNames.Basic).Value;
            var email = httpContext.User.Claims.First(x => x.Type is ClaimTypes.Email).Value;
            using (LogContext.PushProperty("User", email))
            {
                logger.LogInformation(
                    "Getting time data from TP, startTime: {startTime}, endTime: {endTime}, user: {user}", startTime,
                    endTime, userId);

                var result = await mediator.Send(new TimeDataRequest(startTime, endTime, userId, auth),
                    cancellationToken);


                return result.IsFailure
                    ? Results.Problem()
                    : Results.Ok(result.Value.Times);
            }
        }).RequireAuthorization();

        app.MapGet("/api/User", (HttpContext httpContext) => Results.Ok(CreateUserInfo(httpContext.User)));
    }
    
    private UserInfo CreateUserInfo(ClaimsPrincipal claimsPrincipal)
    {
        if (!claimsPrincipal?.Identity?.IsAuthenticated ?? true)
        {
            return UserInfo.Anonymous;
        }

        var userInfo = new UserInfo
        {
            IsAuthenticated = true
        };

        if (claimsPrincipal?.Identity is ClaimsIdentity claimsIdentity)
        {
            userInfo.NameClaimType = claimsIdentity.NameClaimType;
            userInfo.RoleClaimType = claimsIdentity.RoleClaimType;
            userInfo.EmailClaimType = claimsIdentity.Claims.First(x => x.Type is ClaimTypes.Email).Value;
        }
        else
        {
            userInfo.NameClaimType = ClaimTypes.Name;
            userInfo.RoleClaimType = ClaimTypes.Role;
        }

        if (claimsPrincipal?.Claims?.Any() ?? false)
        {
            var claims = claimsPrincipal.Claims.Select(u => new ClaimValue(u.Type, u.Value)).Where(x => x.Type != ClaimNames.Basic)
                .ToList();
            
            userInfo.Claims = claims;
        }

        return userInfo;
    }
}