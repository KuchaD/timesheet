﻿global using System.Diagnostics;
global using System.Security.Claims;

global using TimeShit.WebAssembly.Server;
global using TimeShit.WebAssembly.Server.Services;
global using TimeShit.WebAssembly.Shared.Authorization;
global using TimeShit.WebAssembly.Shared.Defaults;

global using Microsoft.AspNetCore.Authentication;
global using Microsoft.AspNetCore.Authentication.Cookies;
global using Microsoft.AspNetCore.Authentication.OpenIdConnect;
global using Microsoft.AspNetCore.Authorization;
global using Microsoft.AspNetCore.Components.WebAssembly;
global using Microsoft.AspNetCore.Mvc;
global using Microsoft.AspNetCore.Mvc.RazorPages;
global using Microsoft.IdentityModel.Protocols.OpenIdConnect;

