﻿using Autofac;
using Autofac.Extensions.DependencyInjection;
using Carter;
using Serilog;
using Serilog.Extensions.Autofac.DependencyInjection;
using TimeShitApp.Application;
using ILogger = Serilog.ILogger;

var builder = WebApplication.CreateBuilder(args);

var services = builder.Services;
var env = builder.Environment;

builder.Host.UseServiceProviderFactory(new AutofacServiceProviderFactory())
    .ConfigureContainer<ContainerBuilder>(builder =>
    {
        builder.RegisterModule(new AutofacApplicationModule());
        builder.RegisterModule(new AutofacInfrastructureModule());
    });

var loggerConfiguration = new LoggerConfiguration();
loggerConfiguration
    .ReadFrom.Configuration(builder.Configuration)
    .Enrich.FromLogContext()
    .WriteTo.Console()
    .WriteTo.Seq(builder.Configuration.GetRequiredSection("Seq").Value ?? string.Empty);

var logger = loggerConfiguration.CreateLogger();
builder.Services.AddLogging(lb => lb.ClearProviders());
builder.Logging.AddSerilog(logger);
builder.Services.AddSingleton<ILogger>(logger);

services.AddAntiforgery(options =>
{
    options.HeaderName = AntiforgeryDefaults.HeaderName;
    options.Cookie.Name = AntiforgeryDefaults.CookieName;
});

services.AddHttpClient();
services.AddOptions();

builder.Services.AddAuthentication(CookieAuthenticationDefaults.AuthenticationScheme)
    .AddCookie();

services.AddControllersWithViews(options =>
     options.Filters.Add(new AutoValidateAntiforgeryTokenAttribute()));

services.AddRazorPages();

services.AddCarter();

var app = builder.Build();

if (env.IsDevelopment())
{
    app.UseDeveloperExceptionPage();
    app.UseWebAssemblyDebugging();
}
else
{
    app.UseExceptionHandler("/Error");
}

//app.UseSerilogRequestLogging();
app.UseSecurityHeaders(
    SecurityHeadersDefinitions.GetHeaderPolicyCollection(env.IsDevelopment(),"Test"));

app.UseHttpsRedirection();
app.UseBlazorFrameworkFiles();
app.UseStaticFiles();

app.UseRouting();

app.UseNoUnauthorizedRedirect("/api");

app.UseAuthentication();
app.UseAuthorization();

app.MapCarter();
app.MapRazorPages();
app.MapControllers();
app.MapNotFound("/api/{**segment}");
app.MapFallbackToPage("/_Host");

app.Run();
