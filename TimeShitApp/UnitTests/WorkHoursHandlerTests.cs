using Application;
using FluentAssertions;
using PublicHoliday;

namespace UnitTests;

public class UnitTest1
{
    [Fact]
    public async Task Handler_ShouldReturnCorrectHours( )
    {
        var startDay = new DateTime(2023, 4, 1);
        var endDay = new DateTime(2023, 4, 13);

        var request = new WorkHoursRequest(startDay, endDay);
        var handler = new WorkHoursHandler(new CzechRepublicPublicHoliday());
        var result = await handler.Handle(request, CancellationToken.None);

        result.IsSuccess.Should().BeTrue();
        result.Value.WorkDays.Count.Should().Be(7);
        result.Value.Holidays.Count.Should().Be(2);
    }

    [Fact]
    public async Task Handler_2023_ShouldReturnCorrectHours( )
    {
        var startDay = new DateTime(2023, 1, 1);
        var endDay = new DateTime(2023, 12, 31);

        var request = new WorkHoursRequest(startDay, endDay);
        var handler = new WorkHoursHandler(new CzechRepublicPublicHoliday());
        var result = await handler.Handle(request, CancellationToken.None);

        result.IsSuccess.Should().BeTrue();
        result.Value.WorkDays.Count.Should().Be(250);
        result.Value.Holidays.Count.Should().Be(13);
    }
}
