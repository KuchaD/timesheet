using Application;
using FluentAssertions;
using TimeShitApp.Share;

namespace UnitTests;

public class TimeRecalculationHandlerTests
{
    [Theory]
    [InlineData(20, 80)]
    [InlineData(-20, 40)]
    public async Task Handler_ShouldReturnCorrectHours(double correct, double expected)
    {

        var times = new []
        {
            new Time()
            {
                Date = new DateTime(2023, 1, 1) , Hours = 20
            },
            new Time()
            {
                Date = new DateTime(2023, 1, 2) , Hours = 20
            },
            new Time()
            {
                Date = new DateTime(2023, 1, 3) , Hours = 20
            }
        };

        var listTime = times.ToList();
        var request = new TimeRecalculationRequest(listTime, correct, false);
        var handler = new TimeRecalculationHandler();
        var result = await handler.Handle(request, CancellationToken.None);

        result.IsSuccess.Should().BeTrue();

        var resultValue = result.Value.Times.Sum(x => x.Hours);
        resultValue.Should().Be(expected);
    }

    [Fact]
    public async Task Handler_WholeHour_ShouldReturnWholeHours()
    {

        var times = new Time[]
        {
            new ()
            {
                Date = new DateTime(2023, 1, 1) , Hours = 20
            },
            new ()
            {
                Date = new DateTime(2023, 1, 2) , Hours = 20
            },
            new ()
            {
                Date = new DateTime(2023, 1, 3) , Hours = 20
            }
        };

        var listTime = times.ToList();
        var request = new TimeRecalculationRequest(listTime, 10, true);
        var handler = new TimeRecalculationHandler();
        var result = await handler.Handle(request, CancellationToken.None);

        result.IsSuccess.Should().BeTrue();

        var resultValue = result.Value.Times.Sum(x => x.Hours);
        resultValue.Should().Be(70);

        foreach (var time in result.Value.Times)
        {
            var m = time.Hours % 1;
            m.Should().Be(0);
        }
    }

    [Fact]
    public async Task Handler_WholeHourIsFalse_ShouldReturnWholeHours()
    {

        var times = new []
        {
            new Time()
            {
                Date = new DateTime(2023, 1, 1) , Hours = 20
            },
            new Time()
            {
                Date = new DateTime(2023, 1, 2) , Hours = 20
            }
        };

        var listTime = times.ToList();
        var request = new TimeRecalculationRequest(listTime, 1, false);
        var handler = new TimeRecalculationHandler();
        var result = await handler.Handle(request, CancellationToken.None);

        result.IsSuccess.Should().BeTrue();

        var resultValue = result.Value.Times.Sum(x => x.Hours);
        resultValue.Should().Be(41);

        foreach (var time in result.Value.Times)
        {
            var m = time.Hours % 1;
            m.Should().Be(0.5);
        }
    }
}
