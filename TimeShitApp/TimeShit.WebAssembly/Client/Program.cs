﻿using Application;
using Autofac;
using Autofac.Extensions.DependencyInjection;
using BlazorDownloadFile;
using Refit;

var builder = WebAssemblyHostBuilder.CreateDefault(args);

var services = builder.Services;
builder.ConfigureContainer(new AutofacServiceProviderFactory(builder =>
    {
        builder.RegisterModule(new AutofacApplicationClientModule());

    }));

services.AddOptions();
services.AddAuthorizationCore();
services.TryAddSingleton<AuthenticationStateProvider, HostAuthenticationStateProvider>();
services.TryAddSingleton(sp => (HostAuthenticationStateProvider)sp.GetRequiredService<AuthenticationStateProvider>());
services.AddTransient<AuthorizedHandler>();
services.AddAntDesign();

builder.RootComponents.Add<App>("#app");

services.AddHttpClient("default", client =>
{
    client.BaseAddress = new Uri(builder.HostEnvironment.BaseAddress);
    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
});

services.AddHttpClient(AuthDefaults.AuthorizedClientName, client =>
{
    client.BaseAddress = new Uri(builder.HostEnvironment.BaseAddress);
    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
}).AddHttpMessageHandler<AuthorizedHandler>();

services.AddTransient(sp => sp.GetRequiredService<IHttpClientFactory>().CreateClient("default"));
services.AddTransient<IAntiforgeryHttpClientFactory, AntiforgeryHttpClientFactory>();
builder.Services.AddAntDesign();
services.AddRefitClient<IRefitClient>()
    .ConfigureHttpClient(c => { c.BaseAddress = new Uri(builder.HostEnvironment.BaseAddress); })
    .AddHttpMessageHandler<AuthorizedHandler>();

services.AddBlazorDownloadFile();

await builder.Build().RunAsync();
