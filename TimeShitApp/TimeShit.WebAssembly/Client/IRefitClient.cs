using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Refit;
using TimeShit.WebAssembly.Shared.Models;
using TimeShitApp.Share;

namespace TimeShit.WebAssembly.Client;

public interface IRefitClient
{
    [Post("/api/login")]
    Task<IApiResponse> Login([Body]LoginModel loginModel);

    [Get("/api/logout")]
    Task<IApiResponse> Logout();

    [Get("/api/time")]
    Task<IApiResponse<IList<Time>>> Times(DateTime? startTime, DateTime? endTime);
}