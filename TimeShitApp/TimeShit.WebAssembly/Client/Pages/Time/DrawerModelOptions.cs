using Application.Models;
using TimeShitApp.Share;

namespace AntDesign;

public class DrawerModelOptions
{
    public IList<DateTime> WorkDays { get; set; }
    public IList<DateTime> Holidays { get; set; }
    public IList<Vocations> Vocation { get; set; }

    public IList<Time> Times { get; set; }
}
