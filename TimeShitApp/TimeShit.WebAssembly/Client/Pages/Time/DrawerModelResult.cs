using Application.Models;

namespace AntDesign;

public record DrawerModelResult()
{
    public DrawerModelResult(IList<Vocations> Vocation, double AdditionalHours) : this()
    {
        this.Vocation = Vocation;
        this.AdditionalHours = AdditionalHours;
    }
    public IList<Vocations> Vocation { get; set; }
    public double AdditionalHours { get; set; }
};