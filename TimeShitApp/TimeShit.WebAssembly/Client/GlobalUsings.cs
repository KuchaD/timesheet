﻿global using System.Net;
global using System.Net.Http.Headers;
global using System.Net.Http.Json;
global using System.Security.Claims;

global using TimeShit.WebAssembly.Client;
global using TimeShit.WebAssembly.Client.Services;
global using TimeShit.WebAssembly.Shared.Authorization;
global using TimeShit.WebAssembly.Shared.Defaults;

global using Microsoft.AspNetCore.Components;
global using Microsoft.AspNetCore.Components.Authorization;
global using Microsoft.AspNetCore.Components.WebAssembly.Hosting;
global using Microsoft.Extensions.DependencyInjection.Extensions;
global using Microsoft.JSInterop;
global using AntDesign;
global using Application;
