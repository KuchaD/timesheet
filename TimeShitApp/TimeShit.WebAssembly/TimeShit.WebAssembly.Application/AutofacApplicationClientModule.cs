using Autofac;
using Autofac.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection;
using PublicHoliday;
using Application;
using Autofac.Extensions.DependencyInjection;
using MediatR.Extensions.Autofac.DependencyInjection;
using MediatR.Extensions.Autofac.DependencyInjection.Builder;

namespace Application;

public class AutofacApplicationClientModule : Module
{
    protected override void Load(ContainerBuilder builder)
    {
        builder.RegisterType<CzechRepublicPublicHoliday>().AsImplementedInterfaces().SingleInstance();
        var configuration = MediatRConfigurationBuilder
            .Create(typeof(GenerateTimeSheetHandler).Assembly)
            .WithAllOpenGenericHandlerTypesRegistered()
            .WithRegistrationScope(RegistrationScope.Scoped) // currently only supported values are `Transient` and `Scoped`
            .Build();

        builder.RegisterMediatR(configuration);
    }
}