using IdentityManager.Shared.Shared;
using MediatR;
using TimeShitApp.Share;

namespace Application;

public sealed record GenerateTimeSheetRequest(IList<Time> Times) : IRequest<Result<GenerateTimeSheetResponse>>;
