namespace Application;

public sealed record GenerateTimeSheetResponse(Stream Stream);
