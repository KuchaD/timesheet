using IdentityManager.Shared.Shared;
using MediatR;

namespace Application;

public sealed record WorkHoursRequest(DateTime StartDate, DateTime EndDate) : IRequest<Result<WorkHoursResponse>>;
