using Abstraction.Shared;
using IdentityManager.Shared.Shared;
using MediatR;
using PublicHoliday;
namespace Application;

public sealed class WorkHoursHandler : IRequestHandler<WorkHoursRequest, Result<WorkHoursResponse>>
{
    private readonly IPublicHolidays _publicHolidays;

    public WorkHoursHandler(IPublicHolidays publicHolidays)
    {
        _publicHolidays = publicHolidays;
    }

    public async Task<Result<WorkHoursResponse>> Handle(WorkHoursRequest request, CancellationToken cancellationToken)
    {
        try
        {
            var holidays = _publicHolidays.GetHolidaysInDateRange(request.StartDate, request.EndDate);
            var workingDaysInRange = GetDatesBetween(request.StartDate, request.EndDate, holidays);
            return new WorkHoursResponse(workingDaysInRange, holidays.Select(x => x.HolidayDate).ToList());
        }
        catch (Exception e)
        {
            return Result.Failure<WorkHoursResponse>(new Error(e.Message));
        }
    }

    public List<DateTime> GetDatesBetween(DateTime startDate, DateTime endDate, IList<Holiday> holidays)
    {
        List<DateTime> allDates = new List<DateTime>();
        for (DateTime date = startDate; date <= endDate; date = date.AddDays(1))
        {
            if(holidays.Any(x => x.HolidayDate == date))
                continue;

            if (date.DayOfWeek == DayOfWeek.Saturday || date.DayOfWeek == DayOfWeek.Sunday)
                continue;

            allDates.Add(date);
        }
        return allDates.ToList();
    }
}