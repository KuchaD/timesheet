namespace Application;

public sealed record WorkHoursResponse(IList<DateTime> WorkDays, IList<DateTime> Holidays);
