using Application.Models;
using IdentityManager.Shared.Shared;
using MediatR;
using PublicHoliday;

namespace Application;

public sealed class ValidateHoursHandler : IRequestHandler<ValidateHoursRequest, Result<ValidateHoursResponse>>
{
    private readonly IPublicHolidays _publicHolidays;

    public ValidateHoursHandler(IPublicHolidays publicHolidays)
    {
        _publicHolidays = publicHolidays;
    }

    public async Task<Result<ValidateHoursResponse>> Handle(ValidateHoursRequest request, CancellationToken cancellationToken)
    {
        var timeWork = request.Times.Select(x => x.Date).Distinct();
        var missing = request.WorkDays.Except(timeWork);

        var probablyVocation = missing.Select(x =>
        {
            if (_publicHolidays.IsPublicHoliday(x.Date))
            {
                return new Vocations(x.Date){ IsVocation = false, IsHoliday = false };
            }

            return new Vocations(x.Date);
        });

        return new ValidateHoursResponse(probablyVocation.ToList());
    }
}