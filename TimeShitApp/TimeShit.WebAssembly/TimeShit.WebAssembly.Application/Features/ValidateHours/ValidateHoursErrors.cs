using Abstraction.Shared;

namespace Application;

public static class ValidateHoursErrors
{
    public static readonly Error InvalidInput = new ("InvalidInput", "Invalid input");
}
