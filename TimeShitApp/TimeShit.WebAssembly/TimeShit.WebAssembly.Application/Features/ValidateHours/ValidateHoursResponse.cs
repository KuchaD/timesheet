using Application.Models;

namespace Application;

public sealed record ValidateHoursResponse(IList<Vocations> VocationsList);
