using IdentityManager.Shared.Shared;
using MediatR;
using TimeShitApp.Share;

namespace Application;

public sealed record ValidateHoursRequest(IList<DateTime> WorkDays, IList<DateTime> Holiday, IList<Time> Times ) : IRequest<Result<ValidateHoursResponse>>;
