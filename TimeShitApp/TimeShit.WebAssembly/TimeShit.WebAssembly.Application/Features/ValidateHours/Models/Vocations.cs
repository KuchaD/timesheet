namespace Application.Models;

public record Vocations()
{
    public  Vocations(DateTime date) : this()
    {
        Date = date;
        IsVocation = false;
    }

    public DateTime Date { get; init; }
    public bool IsVocation { get; set; }

    public bool IsHoliday { get; init; } = false;
}
