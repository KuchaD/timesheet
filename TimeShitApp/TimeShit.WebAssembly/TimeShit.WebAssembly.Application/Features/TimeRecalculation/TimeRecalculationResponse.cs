using TimeShitApp.Share;

namespace Application;

public sealed record TimeRecalculationResponse(IList<Time> Times);
