using IdentityManager.Shared.Shared;
using MediatR;
using TimeShitApp.Share;

namespace Application;

public sealed record TimeRecalculationRequest(IList<Time> Times, double AdditionHours, bool WholeHours) 
    : IRequest<Result<TimeRecalculationResponse>>;
